<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s3 & s4 Activity</title>
</head>
<body>

	<h1>Person</h1>
	<p><?php echo $person->printName(); ?></p>

	<h1>Developer</h1>
	<p><?php echo $developer->printName(); ?></p>


	<h1>Engineer</h1>
	<p><?php echo $engineer->printName(); ?></p>


	<h1>Building</h1>
	<p>The name of the building is <?php echo $building->getName(); ?>.</p>
	<p>The <?php echo $building->getName(); ?> has <?php echo $building->getFloors(); ?> floors.</p>
	<p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>.</p>

	<?php echo $building->setName('Caswynn Complex'); ?>

	<p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>


	<h1>Condominium</h1>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>
	<p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloors(); ?> floors.</p>
	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>.</p>

	<?php echo $condominium->setName('Enzo Tower'); ?>

	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?>.</p>

</body>
</html>