<?php

// s3
class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($firstName, $middleName, $lastName){
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}

	public function printName() {
		return "Your full name is $this->firstName $this->middleName $this->lastName";
	}
};

$person = new Person('Senku', '', 'Ishigami');


class Developer extends Person {

	public function printName() {
		return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
	}
};

$developer = new Developer('John', 'Finch', 'Smith');


class Engineer extends Person {

	public function printName() {
		return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
	}
};

$engineer = new Engineer('Harold', 'Myers', 'Reese');



// s4
class Building {
	protected $name;
	public $floors;
	public $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}

	public function getfloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		$this->name = $name;
	}
};


$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


class Condominium extends Building {

	public function getName(){
		return $this->name;
	}

	public function getfloors(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		$this->name = $name;
	}
}


$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
